import DrumTrack from './DrumTrack';

export default class DrumKick {
  drumTrack: DrumTrack;
  lengthMS: number;
  constructor(track: DrumTrack, lengthMS: number) {
    this.drumTrack = track;
    this.lengthMS = lengthMS
  }

  play() {
    this.drumTrack.noise.setGain(1);
    setTimeout(this.stop.bind(this), this.lengthMS);
  }

  stop() {
    this.drumTrack.noise.setGain(0);
  }
}
