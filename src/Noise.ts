export default class Noise {
  context: AudioContext;
  gain: GainNode;
  constructor(context: AudioContext) {
    this.context = context;

    const buffer = context.createBuffer(2, context.sampleRate * 3, context.sampleRate);
    this.fillBuffer(buffer);

    this.gain = context.createGain();
    this.gain.gain.value = 0;

    const bufferSource = context.createBufferSource();
    bufferSource.buffer = buffer;
    bufferSource.connect(this.gain);
    bufferSource.loop = true;
    bufferSource.start();
  }

  fillBuffer(buffer: AudioBuffer) {
    for (let channel = 0; channel < buffer.numberOfChannels; channel++) {
      const nowBuffering = buffer.getChannelData(channel);
      for (let i = 0; i < buffer.length; i++) {
        nowBuffering[i] = Math.random() * 2 - 1;
      }
    }
  }

  setGain(gain: number) {
    this.gain.gain.setValueAtTime(gain, this.context.currentTime);
  }

  connect(node: AudioNode) {
    this.gain.connect(node);
  }
}
