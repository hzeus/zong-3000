export const calculateDuration = (
  noteValue: NoteValue,
  tempo: number,
  resolution: NoteValue,
  timing: Timing,
  firstOfPair: boolean,
) => {
  const ticks = noteValue / resolution;
  const swing = !!(ticks % 2);
  const swingFactor = (timing === 'straight' || !swing) ? 1 : (firstOfPair ? 4 / 3 : 2 / 3);
  return (60 * 1000 / tempo) * 4 * noteValue * swingFactor;
}
