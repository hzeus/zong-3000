/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../src/DrumKick.ts":
/*!**************************!*\
  !*** ../src/DrumKick.ts ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DrumKick; });
class DrumKick {
    constructor(track, lengthMS) {
        this.drumTrack = track;
        this.lengthMS = lengthMS;
    }
    play() {
        this.drumTrack.noise.setGain(1);
        setTimeout(this.stop.bind(this), this.lengthMS);
    }
    stop() {
        this.drumTrack.noise.setGain(0);
    }
}


/***/ }),

/***/ "../src/DrumTrack.ts":
/*!***************************!*\
  !*** ../src/DrumTrack.ts ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DrumTrack; });
/* harmony import */ var _DrumKick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DrumKick */ "../src/DrumKick.ts");
/* harmony import */ var _Noise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Noise */ "../src/Noise.ts");


const KICK_LENGTHS = {
    'h': 10,
    's': 100,
};
class DrumTrack {
    constructor(song, kicks) {
        this.song = song;
        this.noise = new _Noise__WEBPACK_IMPORTED_MODULE_1__["default"](song.context);
        this.kicksDefinition = kicks;
    }
    get kicks() {
        return this.kicksDefinition.map(kick => kick ? new _DrumKick__WEBPACK_IMPORTED_MODULE_0__["default"](this, KICK_LENGTHS[kick]) : null);
    }
    playKick(index) {
        const kick = this.kicks[index % this.kicks.length];
        if (kick) {
            kick.play();
        }
    }
}


/***/ }),

/***/ "../src/Noise.ts":
/*!***********************!*\
  !*** ../src/Noise.ts ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Noise; });
class Noise {
    constructor(context) {
        this.context = context;
        const buffer = context.createBuffer(2, context.sampleRate * 3, context.sampleRate);
        this.fillBuffer(buffer);
        this.gain = context.createGain();
        this.gain.gain.value = 0;
        this.gain.connect(context.destination);
        const bufferSource = context.createBufferSource();
        bufferSource.buffer = buffer;
        bufferSource.connect(this.gain);
        bufferSource.loop = true;
        bufferSource.start();
    }
    fillBuffer(buffer) {
        for (let channel = 0; channel < buffer.numberOfChannels; channel++) {
            const nowBuffering = buffer.getChannelData(channel);
            for (let i = 0; i < buffer.length; i++) {
                nowBuffering[i] = Math.random() * 2 - 1;
            }
        }
    }
    setGain(gain) {
        this.gain.gain.setValueAtTime(gain, this.context.currentTime);
    }
}


/***/ }),

/***/ "../src/Note.ts":
/*!**********************!*\
  !*** ../src/Note.ts ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Note; });
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils */ "../src/utils.ts");

const STOP_DURATION = 10;
const NOTES = ['c', 'cs', 'd', 'ds', 'e', 'f', 'fs', 'g', 'gs', 'a', 'as', 'b'];
const FIXED_NOTE = ['a', 4];
const FIXED_FREQUENCY = 440;
const EQUAL_TEMPERED_FACTOR = Math.pow(2, 1 / 12);
class Note {
    constructor(track, note) {
        this.track = track;
        this.note = note;
        this.firstOfPair = false;
    }
    play() {
        this.track.oscillator.setFrequency(this.frequency);
        setTimeout(this.stop.bind(this), this.playLengthMS);
    }
    stop() {
        this.track.oscillator.setFrequency(0);
    }
    get frequency() {
        if (this.note[0] === 'r') {
            return 0;
        }
        return FIXED_FREQUENCY * Math.pow(EQUAL_TEMPERED_FACTOR, this.halfStepsFromFixedNote);
    }
    get halfStepsFromFixedNote() {
        if (this.note[0] === 'r') {
            return 0;
        }
        const halfSteps = NOTES.indexOf(this.note[0][0]) - NOTES.indexOf(FIXED_NOTE[0]);
        const octaveHalfSteps = (this.note[0][1] - FIXED_NOTE[1]) * 12;
        return halfSteps + octaveHalfSteps;
    }
    get duration() {
        return Object(_utils__WEBPACK_IMPORTED_MODULE_0__["calculateDuration"])(this.note[1], this.track.song.tempo, this.track.song.resolution, this.track.song.timing, this.firstOfPair);
    }
    get playLengthMS() {
        return this.duration - STOP_DURATION;
    }
}


/***/ }),

/***/ "../src/Oscillator.ts":
/*!****************************!*\
  !*** ../src/Oscillator.ts ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Oscillator; });
// See: https://developer.mozilla.org/en-US/docs/Web/API/WaveShaperNode/curve#Example
function makeDistortionCurve(amount) {
    var k = typeof amount === 'number' ? amount : 50, n_samples = 44100, curve = new Float32Array(n_samples), deg = Math.PI / 180, i = 0, x;
    for (; i < n_samples; ++i) {
        x = i * 2 / n_samples - 1;
        curve[i] = (3 + k) * x * 20 * deg / (Math.PI + k * Math.abs(x));
    }
    return curve;
}
;
class Oscillator {
    constructor(context, wave) {
        this.context = context;
        const distortion = context.createWaveShaper();
        distortion.curve = makeDistortionCurve(50);
        distortion.oversample = '4x';
        distortion.connect(context.destination);
        this.oscillator = context.createOscillator();
        this.oscillator.connect(distortion);
        this.oscillator.type = wave;
        this.oscillator.frequency.value = 0;
        this.oscillator.start();
    }
    setFrequency(frequency) {
        this.oscillator.frequency.setValueAtTime(frequency, this.context.currentTime);
    }
}


/***/ }),

/***/ "../src/Song.ts":
/*!**********************!*\
  !*** ../src/Song.ts ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Song; });
/* harmony import */ var _Track__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Track */ "../src/Track.ts");
/* harmony import */ var _DrumTrack__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DrumTrack */ "../src/DrumTrack.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils */ "../src/utils.ts");



class Song {
    constructor(song, context) {
        this.context = context;
        this.tempo = song.tempo;
        this.timing = song.timing || 'straight';
        this.resolution = song.resolution;
        this.tracks = song.tracks.map(track => new _Track__WEBPACK_IMPORTED_MODULE_0__["default"](this, track));
        this.drumTrack = new _DrumTrack__WEBPACK_IMPORTED_MODULE_1__["default"](this, song.drums);
        this.index = 0;
    }
    play() {
        this.playNext();
    }
    playNext() {
        if (this.tracks.some(track => track.hasNote(this.index))) {
            this.tracks.forEach(t => t.playNote(this.index));
            this.drumTrack.playKick(this.index);
            this.index++;
            this.playNextTimeout = setTimeout(this.playNext.bind(this), this.tickMilliseconds());
        }
        else {
            this.stop();
        }
    }
    stop() {
        this.pause();
        this.index = 0;
    }
    pause() {
        clearTimeout(this.playNextTimeout);
        this.playNextTimeout = undefined;
    }
    isPlaying() {
        return !!this.playNextTimeout;
    }
    tickMilliseconds() {
        return Object(_utils__WEBPACK_IMPORTED_MODULE_2__["calculateDuration"])(this.resolution, this.tempo, this.resolution, this.timing, !!(this.index % 2));
    }
}


/***/ }),

/***/ "../src/Track.ts":
/*!***********************!*\
  !*** ../src/Track.ts ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Track; });
/* harmony import */ var _Note__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Note */ "../src/Note.ts");
/* harmony import */ var _Oscillator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Oscillator */ "../src/Oscillator.ts");


class Track {
    constructor(song, track) {
        this.song = song;
        this.oscillator = new _Oscillator__WEBPACK_IMPORTED_MODULE_1__["default"](song.context, track.wave);
        this.mute = track.mute || false;
        this.notesDefinition = track.notes;
    }
    get notes() {
        const notes = this.notesDefinition.reduce((notes, note) => {
            const newNote = new _Note__WEBPACK_IMPORTED_MODULE_0__["default"](this, note);
            const nulls = Array(note[1] / this.song.resolution - 1).map(() => null);
            return [...notes, newNote, ...nulls];
        }, []);
        notes.forEach((note, index) => {
            if (note) {
                note.firstOfPair = !!(index % 2);
            }
        });
        return notes;
    }
    hasNote(index) {
        return index < this.notes.length;
    }
    playNote(index) {
        if (!this.mute) {
            const note = this.notes[index];
            if (note) {
                note.play();
            }
        }
    }
}


/***/ }),

/***/ "../src/index.ts":
/*!***********************!*\
  !*** ../src/index.ts ***!
  \***********************/
/*! exports provided: Song */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Song__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Song */ "../src/Song.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Song", function() { return _Song__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./types */ "../src/types.ts");
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_types__WEBPACK_IMPORTED_MODULE_1__);




/***/ }),

/***/ "../src/types.ts":
/*!***********************!*\
  !*** ../src/types.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var NoteValue;
(function (NoteValue) {
    NoteValue[NoteValue["whole"] = 1] = "whole";
    NoteValue[NoteValue["half"] = 0.5] = "half";
    NoteValue[NoteValue["quarter"] = 0.25] = "quarter";
    NoteValue[NoteValue["eighth"] = 0.125] = "eighth";
    NoteValue[NoteValue["sixteenth"] = 0.0625] = "sixteenth";
})(NoteValue || (NoteValue = {}));
var Octave;
(function (Octave) {
    Octave[Octave["subsubcontra"] = -1] = "subsubcontra";
    Octave[Octave["subcontra"] = 0] = "subcontra";
    Octave[Octave["contra"] = 1] = "contra";
    Octave[Octave["great"] = 2] = "great";
    Octave[Octave["small"] = 3] = "small";
    Octave[Octave["onelined"] = 4] = "onelined";
    Octave[Octave["twolined"] = 5] = "twolined";
    Octave[Octave["threelined"] = 6] = "threelined";
    Octave[Octave["fourlined"] = 7] = "fourlined";
    Octave[Octave["fivelined"] = 8] = "fivelined";
    Octave[Octave["sixlined"] = 9] = "sixlined";
})(Octave || (Octave = {}));


/***/ }),

/***/ "../src/utils.ts":
/*!***********************!*\
  !*** ../src/utils.ts ***!
  \***********************/
/*! exports provided: calculateDuration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateDuration", function() { return calculateDuration; });
const calculateDuration = (noteValue, tempo, resolution, timing, firstOfPair) => {
    const ticks = noteValue / resolution;
    const swing = !!(ticks % 2);
    const swingFactor = (timing === 'straight' || !swing) ? 1 : (firstOfPair ? 4 / 3 : 2 / 3);
    return (60 * 1000 / tempo) * 4 * noteValue * swingFactor;
};


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../src/index */ "../src/index.ts");
/* harmony import */ var _let_it_snow_song__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./let-it-snow/song */ "./src/let-it-snow/song.ts");


const AudioContext = (window.AudioContext || window.webkitAudioContext);
const context = new AudioContext();
const song = new _src_index__WEBPACK_IMPORTED_MODULE_0__["Song"](_let_it_snow_song__WEBPACK_IMPORTED_MODULE_1__["default"], context);
const playPause = document.querySelector('[data-playpause]');
if (playPause) {
    playPause.addEventListener('click', function () {
        if (song.isPlaying()) {
            song.pause();
        }
        else {
            song.play();
        }
    });
}
const stop = document.querySelector('[data-stop]');
if (stop) {
    stop.addEventListener('click', function () {
        song.stop();
    });
}
const tempo = document.querySelector('input[data-tempo]');
tempo.value = song.tempo.toString();
if (tempo) {
    tempo.addEventListener('change', function () {
        song.tempo = Number.parseInt(tempo.value);
    });
}
const timing = document.querySelector('select[data-timing]');
timing.value = song.timing;
if (timing) {
    timing.addEventListener('change', function () {
        song.timing = timing.value;
    });
}


/***/ }),

/***/ "./src/let-it-snow/bass.ts":
/*!*********************************!*\
  !*** ./src/let-it-snow/bass.ts ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const bass = {
    wave: 'triangle',
    notes: [
        ['r', 1 / 2],
        ['r', 1 / 2],
        [['c', 2], 1 / 4],
        ['r', 1 / 8],
        [['c', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['c', 2], 1 / 4],
        ['r', 1 / 8],
        [['c', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 8],
        [['a', 2], 1 / 4],
        ['r', 1 / 8],
        [['a', 2], 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 8],
        [['a', 2], 1 / 4],
        ['r', 1 / 8],
        [['a', 2], 1 / 8],
        [['b', 2], 1 / 4],
        [['b', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['c', 2], 1 / 4],
        ['r', 1 / 8],
        [['c', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['c', 2], 1 / 4],
        ['r', 1 / 8],
        [['c', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['c', 2], 1 / 4],
        ['r', 1 / 8],
        [['c', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 8],
        [['a', 2], 1 / 4],
        ['r', 1 / 8],
        [['a', 2], 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 8],
        [['a', 2], 1 / 4],
        ['r', 1 / 8],
        [['a', 2], 1 / 8],
        [['b', 2], 1 / 4],
        [['b', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['c', 2], 1 / 4],
        ['r', 1 / 8],
        [['c', 2], 1 / 8],
        [['g', 2], 1 / 4],
        ['r', 1 / 8],
        [['g', 2], 1 / 8],
        [['g', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['a', 2], 1 / 4],
        [['a', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['g', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['d', 2], 1 / 4],
        [['c', 2], 1 / 4],
        ['r', 1 / 8],
        [['d', 2], 1 / 4],
        ['r', 1 / 4],
        [['g', 2], 1 / 4],
    ]
};
/* harmony default export */ __webpack_exports__["default"] = (bass);


/***/ }),

/***/ "./src/let-it-snow/lead.ts":
/*!*********************************!*\
  !*** ./src/let-it-snow/lead.ts ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const lead = {
    wave: 'square',
    notes: [
        ['r', 1 / 2],
        ['r', 1 / 4],
        [['g', 3], 1 / 8],
        [['g', 3], 1 / 8],
        [['g', 4], 1 / 8],
        [['g', 4], 1 / 8],
        [['f', 4], 1 / 4],
        [['e', 4], 1 / 4],
        [['d', 4], 1 / 8],
        [['c', 4], 1 / 4],
        [['g', 3], 1 / 4],
        ['r', 1 / 4],
        ['r', 1 / 8],
        [['c', 4], 1 / 8],
        [['c', 4], 1 / 8],
        [['d', 4], 1 / 4],
        ['r', 1 / 8],
        [['d', 4], 1 / 8],
        [['d', 4], 1 / 4],
        [['c', 4], 1 / 8],
        [['b', 3], 1 / 4],
        [['g', 3], 1 / 4],
        ['r', 1 / 4],
        ['r', 1 / 8],
        [['a', 3], 1 / 8],
        [['a', 3], 1 / 8],
        [['a', 4], 1 / 8],
        [['a', 4], 1 / 8],
        [['g', 4], 1 / 4],
        [['f', 4], 1 / 4],
        [['e', 4], 1 / 8],
        [['d', 4], 1 / 4],
        ['r', 1 / 2],
        [['b', 4], 1 / 8],
        [['a', 4], 1 / 8],
        [['g', 4], 1 / 4],
        [['g', 4], 1 / 8],
        [['f', 4], 1 / 8],
        [['e', 4], 1 / 4],
        [['e', 4], 1 / 8],
        [['d', 4], 1 / 8],
        [['c', 4], 1 / 4],
        ['r', 1 / 8],
        ['r', 1 / 2],
        [['g', 3], 1 / 8],
        [['g', 3], 1 / 8],
        [['g', 4], 1 / 8],
        [['g', 4], 1 / 8],
        [['f', 4], 1 / 4],
        [['e', 4], 1 / 4],
        [['d', 4], 1 / 8],
        [['c', 4], 1 / 4],
        [['g', 3], 1 / 4],
        ['r', 1 / 4],
        ['r', 1 / 8],
        [['c', 4], 1 / 8],
        [['c', 4], 1 / 8],
        [['d', 4], 1 / 4],
        ['r', 1 / 8],
        [['d', 4], 1 / 8],
        [['d', 4], 1 / 4],
        [['c', 4], 1 / 8],
        [['b', 3], 1 / 4],
        [['g', 3], 1 / 4],
        ['r', 1 / 4],
        ['r', 1 / 8],
        [['a', 3], 1 / 8],
        [['a', 3], 1 / 8],
        [['a', 4], 1 / 8],
        [['a', 4], 1 / 8],
        [['g', 4], 1 / 4],
        [['f', 4], 1 / 4],
        [['e', 4], 1 / 8],
        [['d', 4], 1 / 4],
        ['r', 1 / 2],
        [['b', 4], 1 / 8],
        [['a', 4], 1 / 8],
        [['g', 4], 1 / 4],
        [['g', 4], 1 / 8],
        [['f', 4], 1 / 8],
        [['e', 4], 1 / 4],
        [['e', 4], 1 / 8],
        [['d', 4], 1 / 8],
        [['c', 4], 1 / 4],
        ['r', 1 / 8],
        ['r', 1 / 2],
        [['b', 3], 1 / 8],
        [['c', 4], 1 / 8],
        [['d', 4], 1 / 8],
        [['e', 4], 1 / 8],
        [['d', 4], 1 / 4],
        [['b', 3], 1 / 4],
        [['g', 4], 1 / 8],
        [['d', 4], 1 / 4],
        ['r', 1 / 2],
        [['b', 3], 1 / 8],
        [['d', 4], 1 / 8],
        [['c', 4], 1 / 4],
        [['c', 4], 1 / 8],
        [['b', 3], 1 / 8],
        [['a', 3], 1 / 4],
        [['a', 3], 1 / 8],
        [['c', 4], 1 / 8],
        [['b', 3], 1 / 4],
        ['r', 1 / 2],
        ['r', 1 / 8],
        [['b', 3], 1 / 8],
        [['c', 4], 1 / 8],
        [['d', 4], 1 / 8],
        [['e', 4], 1 / 8],
        [['d', 4], 1 / 4],
        [['b', 3], 1 / 4],
        [['g', 4], 1 / 8],
        [['d', 4], 1 / 4],
        ['r', 1 / 2],
        ['r', 1 / 4],
        ['r', 1 / 8],
        [['g', 4], 1 / 8],
        [['fs', 4], 1 / 8],
        [['e', 4], 1 / 8],
        [['fs', 4], 1 / 4],
        [['e', 4], 1 / 8],
        [['d', 4], 1 / 8],
        [['g', 4], 1 / 4],
    ],
};
/* harmony default export */ __webpack_exports__["default"] = (lead);


/***/ }),

/***/ "./src/let-it-snow/song.ts":
/*!*********************************!*\
  !*** ./src/let-it-snow/song.ts ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _lead__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./lead */ "./src/let-it-snow/lead.ts");
/* harmony import */ var _bass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bass */ "./src/let-it-snow/bass.ts");


const song = {
    tempo: 140,
    resolution: 1 / 8,
    timing: 'swing',
    tracks: [
        _lead__WEBPACK_IMPORTED_MODULE_0__["default"],
        _bass__WEBPACK_IMPORTED_MODULE_1__["default"],
    ],
    drums: ['h', 'h', 's', 'h', 'h', 'h', 's', 'h'],
};
/* harmony default export */ __webpack_exports__["default"] = (song);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4uL3NyYy9EcnVtS2ljay50cyIsIndlYnBhY2s6Ly8vLi4vc3JjL0RydW1UcmFjay50cyIsIndlYnBhY2s6Ly8vLi4vc3JjL05vaXNlLnRzIiwid2VicGFjazovLy8uLi9zcmMvTm90ZS50cyIsIndlYnBhY2s6Ly8vLi4vc3JjL09zY2lsbGF0b3IudHMiLCJ3ZWJwYWNrOi8vLy4uL3NyYy9Tb25nLnRzIiwid2VicGFjazovLy8uLi9zcmMvVHJhY2sudHMiLCJ3ZWJwYWNrOi8vLy4uL3NyYy9pbmRleC50cyIsIndlYnBhY2s6Ly8vLi4vc3JjL3R5cGVzLnRzIiwid2VicGFjazovLy8uLi9zcmMvdXRpbHMudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LnRzIiwid2VicGFjazovLy8uL3NyYy9sZXQtaXQtc25vdy9iYXNzLnRzIiwid2VicGFjazovLy8uL3NyYy9sZXQtaXQtc25vdy9sZWFkLnRzIiwid2VicGFjazovLy8uL3NyYy9sZXQtaXQtc25vdy9zb25nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDWkE7QUFBQTtBQUFBO0FBQUE7QUFBa0M7QUFDTjtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNlO0FBQ2Y7QUFDQTtBQUNBLHlCQUF5Qiw4Q0FBSztBQUM5QjtBQUNBO0FBQ0E7QUFDQSwyREFBMkQsaURBQVE7QUFDbkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ3JCQTtBQUFBO0FBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLG1DQUFtQztBQUNoRTtBQUNBLDJCQUEyQixtQkFBbUI7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ3pCQTtBQUFBO0FBQUE7QUFBNEM7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxnRUFBaUI7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ3ZDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0EsVUFBVSxlQUFlO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUMxQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUE0QjtBQUNRO0FBQ1E7QUFDN0I7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbURBQW1ELDhDQUFLO0FBQ3hELDZCQUE2QixrREFBUztBQUN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsZ0VBQWlCO0FBQ2hDO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN6Q0E7QUFBQTtBQUFBO0FBQUE7QUFBMEI7QUFDWTtBQUN2QjtBQUNmO0FBQ0E7QUFDQSw4QkFBOEIsbURBQVU7QUFDeEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQyw2Q0FBSTtBQUNwQztBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXlDO0FBQ3hCOzs7Ozs7Ozs7Ozs7O0FDREo7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsOEJBQThCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyx3QkFBd0I7Ozs7Ozs7Ozs7Ozs7QUN0QnpCO0FBQUE7QUFBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNMQTtBQUFBO0FBQUE7QUFBdUM7QUFDUztBQUNoRDtBQUNBO0FBQ0EsaUJBQWlCLCtDQUFJLENBQUMseURBQWM7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7Ozs7Ozs7Ozs7OztBQ25DQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDZSxtRUFBSSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDaElwQjtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNlLG1FQUFJLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNqSXBCO0FBQUE7QUFBQTtBQUEwQjtBQUNBO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLDZDQUFJO0FBQ1osUUFBUSw2Q0FBSTtBQUNaO0FBQ0E7QUFDQTtBQUNlLG1FQUFJLEVBQUMiLCJmaWxlIjoiZXhhbXBsZXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC50c1wiKTtcbiIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIERydW1LaWNrIHtcbiAgICBjb25zdHJ1Y3Rvcih0cmFjaywgbGVuZ3RoTVMpIHtcbiAgICAgICAgdGhpcy5kcnVtVHJhY2sgPSB0cmFjaztcbiAgICAgICAgdGhpcy5sZW5ndGhNUyA9IGxlbmd0aE1TO1xuICAgIH1cbiAgICBwbGF5KCkge1xuICAgICAgICB0aGlzLmRydW1UcmFjay5ub2lzZS5zZXRHYWluKDEpO1xuICAgICAgICBzZXRUaW1lb3V0KHRoaXMuc3RvcC5iaW5kKHRoaXMpLCB0aGlzLmxlbmd0aE1TKTtcbiAgICB9XG4gICAgc3RvcCgpIHtcbiAgICAgICAgdGhpcy5kcnVtVHJhY2subm9pc2Uuc2V0R2FpbigwKTtcbiAgICB9XG59XG4iLCJpbXBvcnQgRHJ1bUtpY2sgZnJvbSAnLi9EcnVtS2ljayc7XG5pbXBvcnQgTm9pc2UgZnJvbSAnLi9Ob2lzZSc7XG5jb25zdCBLSUNLX0xFTkdUSFMgPSB7XG4gICAgJ2gnOiAxMCxcbiAgICAncyc6IDEwMCxcbn07XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEcnVtVHJhY2sge1xuICAgIGNvbnN0cnVjdG9yKHNvbmcsIGtpY2tzKSB7XG4gICAgICAgIHRoaXMuc29uZyA9IHNvbmc7XG4gICAgICAgIHRoaXMubm9pc2UgPSBuZXcgTm9pc2Uoc29uZy5jb250ZXh0KTtcbiAgICAgICAgdGhpcy5raWNrc0RlZmluaXRpb24gPSBraWNrcztcbiAgICB9XG4gICAgZ2V0IGtpY2tzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5raWNrc0RlZmluaXRpb24ubWFwKGtpY2sgPT4ga2ljayA/IG5ldyBEcnVtS2ljayh0aGlzLCBLSUNLX0xFTkdUSFNba2lja10pIDogbnVsbCk7XG4gICAgfVxuICAgIHBsYXlLaWNrKGluZGV4KSB7XG4gICAgICAgIGNvbnN0IGtpY2sgPSB0aGlzLmtpY2tzW2luZGV4ICUgdGhpcy5raWNrcy5sZW5ndGhdO1xuICAgICAgICBpZiAoa2ljaykge1xuICAgICAgICAgICAga2ljay5wbGF5KCk7XG4gICAgICAgIH1cbiAgICB9XG59XG4iLCJleHBvcnQgZGVmYXVsdCBjbGFzcyBOb2lzZSB7XG4gICAgY29uc3RydWN0b3IoY29udGV4dCkge1xuICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICBjb25zdCBidWZmZXIgPSBjb250ZXh0LmNyZWF0ZUJ1ZmZlcigyLCBjb250ZXh0LnNhbXBsZVJhdGUgKiAzLCBjb250ZXh0LnNhbXBsZVJhdGUpO1xuICAgICAgICB0aGlzLmZpbGxCdWZmZXIoYnVmZmVyKTtcbiAgICAgICAgdGhpcy5nYWluID0gY29udGV4dC5jcmVhdGVHYWluKCk7XG4gICAgICAgIHRoaXMuZ2Fpbi5nYWluLnZhbHVlID0gMDtcbiAgICAgICAgdGhpcy5nYWluLmNvbm5lY3QoY29udGV4dC5kZXN0aW5hdGlvbik7XG4gICAgICAgIGNvbnN0IGJ1ZmZlclNvdXJjZSA9IGNvbnRleHQuY3JlYXRlQnVmZmVyU291cmNlKCk7XG4gICAgICAgIGJ1ZmZlclNvdXJjZS5idWZmZXIgPSBidWZmZXI7XG4gICAgICAgIGJ1ZmZlclNvdXJjZS5jb25uZWN0KHRoaXMuZ2Fpbik7XG4gICAgICAgIGJ1ZmZlclNvdXJjZS5sb29wID0gdHJ1ZTtcbiAgICAgICAgYnVmZmVyU291cmNlLnN0YXJ0KCk7XG4gICAgfVxuICAgIGZpbGxCdWZmZXIoYnVmZmVyKSB7XG4gICAgICAgIGZvciAobGV0IGNoYW5uZWwgPSAwOyBjaGFubmVsIDwgYnVmZmVyLm51bWJlck9mQ2hhbm5lbHM7IGNoYW5uZWwrKykge1xuICAgICAgICAgICAgY29uc3Qgbm93QnVmZmVyaW5nID0gYnVmZmVyLmdldENoYW5uZWxEYXRhKGNoYW5uZWwpO1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBidWZmZXIubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBub3dCdWZmZXJpbmdbaV0gPSBNYXRoLnJhbmRvbSgpICogMiAtIDE7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgc2V0R2FpbihnYWluKSB7XG4gICAgICAgIHRoaXMuZ2Fpbi5nYWluLnNldFZhbHVlQXRUaW1lKGdhaW4sIHRoaXMuY29udGV4dC5jdXJyZW50VGltZSk7XG4gICAgfVxufVxuIiwiaW1wb3J0IHsgY2FsY3VsYXRlRHVyYXRpb24gfSBmcm9tICcuL3V0aWxzJztcbmNvbnN0IFNUT1BfRFVSQVRJT04gPSAxMDtcbmNvbnN0IE5PVEVTID0gWydjJywgJ2NzJywgJ2QnLCAnZHMnLCAnZScsICdmJywgJ2ZzJywgJ2cnLCAnZ3MnLCAnYScsICdhcycsICdiJ107XG5jb25zdCBGSVhFRF9OT1RFID0gWydhJywgNF07XG5jb25zdCBGSVhFRF9GUkVRVUVOQ1kgPSA0NDA7XG5jb25zdCBFUVVBTF9URU1QRVJFRF9GQUNUT1IgPSBNYXRoLnBvdygyLCAxIC8gMTIpO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTm90ZSB7XG4gICAgY29uc3RydWN0b3IodHJhY2ssIG5vdGUpIHtcbiAgICAgICAgdGhpcy50cmFjayA9IHRyYWNrO1xuICAgICAgICB0aGlzLm5vdGUgPSBub3RlO1xuICAgICAgICB0aGlzLmZpcnN0T2ZQYWlyID0gZmFsc2U7XG4gICAgfVxuICAgIHBsYXkoKSB7XG4gICAgICAgIHRoaXMudHJhY2sub3NjaWxsYXRvci5zZXRGcmVxdWVuY3kodGhpcy5mcmVxdWVuY3kpO1xuICAgICAgICBzZXRUaW1lb3V0KHRoaXMuc3RvcC5iaW5kKHRoaXMpLCB0aGlzLnBsYXlMZW5ndGhNUyk7XG4gICAgfVxuICAgIHN0b3AoKSB7XG4gICAgICAgIHRoaXMudHJhY2sub3NjaWxsYXRvci5zZXRGcmVxdWVuY3koMCk7XG4gICAgfVxuICAgIGdldCBmcmVxdWVuY3koKSB7XG4gICAgICAgIGlmICh0aGlzLm5vdGVbMF0gPT09ICdyJykge1xuICAgICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIEZJWEVEX0ZSRVFVRU5DWSAqIE1hdGgucG93KEVRVUFMX1RFTVBFUkVEX0ZBQ1RPUiwgdGhpcy5oYWxmU3RlcHNGcm9tRml4ZWROb3RlKTtcbiAgICB9XG4gICAgZ2V0IGhhbGZTdGVwc0Zyb21GaXhlZE5vdGUoKSB7XG4gICAgICAgIGlmICh0aGlzLm5vdGVbMF0gPT09ICdyJykge1xuICAgICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgaGFsZlN0ZXBzID0gTk9URVMuaW5kZXhPZih0aGlzLm5vdGVbMF1bMF0pIC0gTk9URVMuaW5kZXhPZihGSVhFRF9OT1RFWzBdKTtcbiAgICAgICAgY29uc3Qgb2N0YXZlSGFsZlN0ZXBzID0gKHRoaXMubm90ZVswXVsxXSAtIEZJWEVEX05PVEVbMV0pICogMTI7XG4gICAgICAgIHJldHVybiBoYWxmU3RlcHMgKyBvY3RhdmVIYWxmU3RlcHM7XG4gICAgfVxuICAgIGdldCBkdXJhdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIGNhbGN1bGF0ZUR1cmF0aW9uKHRoaXMubm90ZVsxXSwgdGhpcy50cmFjay5zb25nLnRlbXBvLCB0aGlzLnRyYWNrLnNvbmcucmVzb2x1dGlvbiwgdGhpcy50cmFjay5zb25nLnRpbWluZywgdGhpcy5maXJzdE9mUGFpcik7XG4gICAgfVxuICAgIGdldCBwbGF5TGVuZ3RoTVMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmR1cmF0aW9uIC0gU1RPUF9EVVJBVElPTjtcbiAgICB9XG59XG4iLCIvLyBTZWU6IGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0FQSS9XYXZlU2hhcGVyTm9kZS9jdXJ2ZSNFeGFtcGxlXG5mdW5jdGlvbiBtYWtlRGlzdG9ydGlvbkN1cnZlKGFtb3VudCkge1xuICAgIHZhciBrID0gdHlwZW9mIGFtb3VudCA9PT0gJ251bWJlcicgPyBhbW91bnQgOiA1MCwgbl9zYW1wbGVzID0gNDQxMDAsIGN1cnZlID0gbmV3IEZsb2F0MzJBcnJheShuX3NhbXBsZXMpLCBkZWcgPSBNYXRoLlBJIC8gMTgwLCBpID0gMCwgeDtcbiAgICBmb3IgKDsgaSA8IG5fc2FtcGxlczsgKytpKSB7XG4gICAgICAgIHggPSBpICogMiAvIG5fc2FtcGxlcyAtIDE7XG4gICAgICAgIGN1cnZlW2ldID0gKDMgKyBrKSAqIHggKiAyMCAqIGRlZyAvIChNYXRoLlBJICsgayAqIE1hdGguYWJzKHgpKTtcbiAgICB9XG4gICAgcmV0dXJuIGN1cnZlO1xufVxuO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3NjaWxsYXRvciB7XG4gICAgY29uc3RydWN0b3IoY29udGV4dCwgd2F2ZSkge1xuICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICBjb25zdCBkaXN0b3J0aW9uID0gY29udGV4dC5jcmVhdGVXYXZlU2hhcGVyKCk7XG4gICAgICAgIGRpc3RvcnRpb24uY3VydmUgPSBtYWtlRGlzdG9ydGlvbkN1cnZlKDUwKTtcbiAgICAgICAgZGlzdG9ydGlvbi5vdmVyc2FtcGxlID0gJzR4JztcbiAgICAgICAgZGlzdG9ydGlvbi5jb25uZWN0KGNvbnRleHQuZGVzdGluYXRpb24pO1xuICAgICAgICB0aGlzLm9zY2lsbGF0b3IgPSBjb250ZXh0LmNyZWF0ZU9zY2lsbGF0b3IoKTtcbiAgICAgICAgdGhpcy5vc2NpbGxhdG9yLmNvbm5lY3QoZGlzdG9ydGlvbik7XG4gICAgICAgIHRoaXMub3NjaWxsYXRvci50eXBlID0gd2F2ZTtcbiAgICAgICAgdGhpcy5vc2NpbGxhdG9yLmZyZXF1ZW5jeS52YWx1ZSA9IDA7XG4gICAgICAgIHRoaXMub3NjaWxsYXRvci5zdGFydCgpO1xuICAgIH1cbiAgICBzZXRGcmVxdWVuY3koZnJlcXVlbmN5KSB7XG4gICAgICAgIHRoaXMub3NjaWxsYXRvci5mcmVxdWVuY3kuc2V0VmFsdWVBdFRpbWUoZnJlcXVlbmN5LCB0aGlzLmNvbnRleHQuY3VycmVudFRpbWUpO1xuICAgIH1cbn1cbiIsImltcG9ydCBUcmFjayBmcm9tICcuL1RyYWNrJztcbmltcG9ydCBEcnVtVHJhY2sgZnJvbSAnLi9EcnVtVHJhY2snO1xuaW1wb3J0IHsgY2FsY3VsYXRlRHVyYXRpb24gfSBmcm9tICcuL3V0aWxzJztcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNvbmcge1xuICAgIGNvbnN0cnVjdG9yKHNvbmcsIGNvbnRleHQpIHtcbiAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgdGhpcy50ZW1wbyA9IHNvbmcudGVtcG87XG4gICAgICAgIHRoaXMudGltaW5nID0gc29uZy50aW1pbmcgfHwgJ3N0cmFpZ2h0JztcbiAgICAgICAgdGhpcy5yZXNvbHV0aW9uID0gc29uZy5yZXNvbHV0aW9uO1xuICAgICAgICB0aGlzLnRyYWNrcyA9IHNvbmcudHJhY2tzLm1hcCh0cmFjayA9PiBuZXcgVHJhY2sodGhpcywgdHJhY2spKTtcbiAgICAgICAgdGhpcy5kcnVtVHJhY2sgPSBuZXcgRHJ1bVRyYWNrKHRoaXMsIHNvbmcuZHJ1bXMpO1xuICAgICAgICB0aGlzLmluZGV4ID0gMDtcbiAgICB9XG4gICAgcGxheSgpIHtcbiAgICAgICAgdGhpcy5wbGF5TmV4dCgpO1xuICAgIH1cbiAgICBwbGF5TmV4dCgpIHtcbiAgICAgICAgaWYgKHRoaXMudHJhY2tzLnNvbWUodHJhY2sgPT4gdHJhY2suaGFzTm90ZSh0aGlzLmluZGV4KSkpIHtcbiAgICAgICAgICAgIHRoaXMudHJhY2tzLmZvckVhY2godCA9PiB0LnBsYXlOb3RlKHRoaXMuaW5kZXgpKTtcbiAgICAgICAgICAgIHRoaXMuZHJ1bVRyYWNrLnBsYXlLaWNrKHRoaXMuaW5kZXgpO1xuICAgICAgICAgICAgdGhpcy5pbmRleCsrO1xuICAgICAgICAgICAgdGhpcy5wbGF5TmV4dFRpbWVvdXQgPSBzZXRUaW1lb3V0KHRoaXMucGxheU5leHQuYmluZCh0aGlzKSwgdGhpcy50aWNrTWlsbGlzZWNvbmRzKCkpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zdG9wKCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgc3RvcCgpIHtcbiAgICAgICAgdGhpcy5wYXVzZSgpO1xuICAgICAgICB0aGlzLmluZGV4ID0gMDtcbiAgICB9XG4gICAgcGF1c2UoKSB7XG4gICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnBsYXlOZXh0VGltZW91dCk7XG4gICAgICAgIHRoaXMucGxheU5leHRUaW1lb3V0ID0gdW5kZWZpbmVkO1xuICAgIH1cbiAgICBpc1BsYXlpbmcoKSB7XG4gICAgICAgIHJldHVybiAhIXRoaXMucGxheU5leHRUaW1lb3V0O1xuICAgIH1cbiAgICB0aWNrTWlsbGlzZWNvbmRzKCkge1xuICAgICAgICByZXR1cm4gY2FsY3VsYXRlRHVyYXRpb24odGhpcy5yZXNvbHV0aW9uLCB0aGlzLnRlbXBvLCB0aGlzLnJlc29sdXRpb24sIHRoaXMudGltaW5nLCAhISh0aGlzLmluZGV4ICUgMikpO1xuICAgIH1cbn1cbiIsImltcG9ydCBOb3RlIGZyb20gJy4vTm90ZSc7XG5pbXBvcnQgT3NjaWxsYXRvciBmcm9tICcuL09zY2lsbGF0b3InO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVHJhY2sge1xuICAgIGNvbnN0cnVjdG9yKHNvbmcsIHRyYWNrKSB7XG4gICAgICAgIHRoaXMuc29uZyA9IHNvbmc7XG4gICAgICAgIHRoaXMub3NjaWxsYXRvciA9IG5ldyBPc2NpbGxhdG9yKHNvbmcuY29udGV4dCwgdHJhY2sud2F2ZSk7XG4gICAgICAgIHRoaXMubXV0ZSA9IHRyYWNrLm11dGUgfHwgZmFsc2U7XG4gICAgICAgIHRoaXMubm90ZXNEZWZpbml0aW9uID0gdHJhY2subm90ZXM7XG4gICAgfVxuICAgIGdldCBub3RlcygpIHtcbiAgICAgICAgY29uc3Qgbm90ZXMgPSB0aGlzLm5vdGVzRGVmaW5pdGlvbi5yZWR1Y2UoKG5vdGVzLCBub3RlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBuZXdOb3RlID0gbmV3IE5vdGUodGhpcywgbm90ZSk7XG4gICAgICAgICAgICBjb25zdCBudWxscyA9IEFycmF5KG5vdGVbMV0gLyB0aGlzLnNvbmcucmVzb2x1dGlvbiAtIDEpLm1hcCgoKSA9PiBudWxsKTtcbiAgICAgICAgICAgIHJldHVybiBbLi4ubm90ZXMsIG5ld05vdGUsIC4uLm51bGxzXTtcbiAgICAgICAgfSwgW10pO1xuICAgICAgICBub3Rlcy5mb3JFYWNoKChub3RlLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgaWYgKG5vdGUpIHtcbiAgICAgICAgICAgICAgICBub3RlLmZpcnN0T2ZQYWlyID0gISEoaW5kZXggJSAyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBub3RlcztcbiAgICB9XG4gICAgaGFzTm90ZShpbmRleCkge1xuICAgICAgICByZXR1cm4gaW5kZXggPCB0aGlzLm5vdGVzLmxlbmd0aDtcbiAgICB9XG4gICAgcGxheU5vdGUoaW5kZXgpIHtcbiAgICAgICAgaWYgKCF0aGlzLm11dGUpIHtcbiAgICAgICAgICAgIGNvbnN0IG5vdGUgPSB0aGlzLm5vdGVzW2luZGV4XTtcbiAgICAgICAgICAgIGlmIChub3RlKSB7XG4gICAgICAgICAgICAgICAgbm90ZS5wbGF5KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG4iLCJleHBvcnQgeyBkZWZhdWx0IGFzIFNvbmcgfSBmcm9tICcuL1NvbmcnO1xuaW1wb3J0ICcuL3R5cGVzJztcbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIE5vdGVWYWx1ZTtcbihmdW5jdGlvbiAoTm90ZVZhbHVlKSB7XG4gICAgTm90ZVZhbHVlW05vdGVWYWx1ZVtcIndob2xlXCJdID0gMV0gPSBcIndob2xlXCI7XG4gICAgTm90ZVZhbHVlW05vdGVWYWx1ZVtcImhhbGZcIl0gPSAwLjVdID0gXCJoYWxmXCI7XG4gICAgTm90ZVZhbHVlW05vdGVWYWx1ZVtcInF1YXJ0ZXJcIl0gPSAwLjI1XSA9IFwicXVhcnRlclwiO1xuICAgIE5vdGVWYWx1ZVtOb3RlVmFsdWVbXCJlaWdodGhcIl0gPSAwLjEyNV0gPSBcImVpZ2h0aFwiO1xuICAgIE5vdGVWYWx1ZVtOb3RlVmFsdWVbXCJzaXh0ZWVudGhcIl0gPSAwLjA2MjVdID0gXCJzaXh0ZWVudGhcIjtcbn0pKE5vdGVWYWx1ZSB8fCAoTm90ZVZhbHVlID0ge30pKTtcbnZhciBPY3RhdmU7XG4oZnVuY3Rpb24gKE9jdGF2ZSkge1xuICAgIE9jdGF2ZVtPY3RhdmVbXCJzdWJzdWJjb250cmFcIl0gPSAtMV0gPSBcInN1YnN1YmNvbnRyYVwiO1xuICAgIE9jdGF2ZVtPY3RhdmVbXCJzdWJjb250cmFcIl0gPSAwXSA9IFwic3ViY29udHJhXCI7XG4gICAgT2N0YXZlW09jdGF2ZVtcImNvbnRyYVwiXSA9IDFdID0gXCJjb250cmFcIjtcbiAgICBPY3RhdmVbT2N0YXZlW1wiZ3JlYXRcIl0gPSAyXSA9IFwiZ3JlYXRcIjtcbiAgICBPY3RhdmVbT2N0YXZlW1wic21hbGxcIl0gPSAzXSA9IFwic21hbGxcIjtcbiAgICBPY3RhdmVbT2N0YXZlW1wib25lbGluZWRcIl0gPSA0XSA9IFwib25lbGluZWRcIjtcbiAgICBPY3RhdmVbT2N0YXZlW1widHdvbGluZWRcIl0gPSA1XSA9IFwidHdvbGluZWRcIjtcbiAgICBPY3RhdmVbT2N0YXZlW1widGhyZWVsaW5lZFwiXSA9IDZdID0gXCJ0aHJlZWxpbmVkXCI7XG4gICAgT2N0YXZlW09jdGF2ZVtcImZvdXJsaW5lZFwiXSA9IDddID0gXCJmb3VybGluZWRcIjtcbiAgICBPY3RhdmVbT2N0YXZlW1wiZml2ZWxpbmVkXCJdID0gOF0gPSBcImZpdmVsaW5lZFwiO1xuICAgIE9jdGF2ZVtPY3RhdmVbXCJzaXhsaW5lZFwiXSA9IDldID0gXCJzaXhsaW5lZFwiO1xufSkoT2N0YXZlIHx8IChPY3RhdmUgPSB7fSkpO1xuIiwiZXhwb3J0IGNvbnN0IGNhbGN1bGF0ZUR1cmF0aW9uID0gKG5vdGVWYWx1ZSwgdGVtcG8sIHJlc29sdXRpb24sIHRpbWluZywgZmlyc3RPZlBhaXIpID0+IHtcbiAgICBjb25zdCB0aWNrcyA9IG5vdGVWYWx1ZSAvIHJlc29sdXRpb247XG4gICAgY29uc3Qgc3dpbmcgPSAhISh0aWNrcyAlIDIpO1xuICAgIGNvbnN0IHN3aW5nRmFjdG9yID0gKHRpbWluZyA9PT0gJ3N0cmFpZ2h0JyB8fCAhc3dpbmcpID8gMSA6IChmaXJzdE9mUGFpciA/IDQgLyAzIDogMiAvIDMpO1xuICAgIHJldHVybiAoNjAgKiAxMDAwIC8gdGVtcG8pICogNCAqIG5vdGVWYWx1ZSAqIHN3aW5nRmFjdG9yO1xufTtcbiIsImltcG9ydCB7IFNvbmcgfSBmcm9tICcuLi8uLi9zcmMvaW5kZXgnO1xuaW1wb3J0IHNvbmdEZWZpbml0aW9uIGZyb20gJy4vbGV0LWl0LXNub3cvc29uZyc7XG5jb25zdCBBdWRpb0NvbnRleHQgPSAod2luZG93LkF1ZGlvQ29udGV4dCB8fCB3aW5kb3cud2Via2l0QXVkaW9Db250ZXh0KTtcbmNvbnN0IGNvbnRleHQgPSBuZXcgQXVkaW9Db250ZXh0KCk7XG5jb25zdCBzb25nID0gbmV3IFNvbmcoc29uZ0RlZmluaXRpb24sIGNvbnRleHQpO1xuY29uc3QgcGxheVBhdXNlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignW2RhdGEtcGxheXBhdXNlXScpO1xuaWYgKHBsYXlQYXVzZSkge1xuICAgIHBsYXlQYXVzZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHNvbmcuaXNQbGF5aW5nKCkpIHtcbiAgICAgICAgICAgIHNvbmcucGF1c2UoKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHNvbmcucGxheSgpO1xuICAgICAgICB9XG4gICAgfSk7XG59XG5jb25zdCBzdG9wID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignW2RhdGEtc3RvcF0nKTtcbmlmIChzdG9wKSB7XG4gICAgc3RvcC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc29uZy5zdG9wKCk7XG4gICAgfSk7XG59XG5jb25zdCB0ZW1wbyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2lucHV0W2RhdGEtdGVtcG9dJyk7XG50ZW1wby52YWx1ZSA9IHNvbmcudGVtcG8udG9TdHJpbmcoKTtcbmlmICh0ZW1wbykge1xuICAgIHRlbXBvLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc29uZy50ZW1wbyA9IE51bWJlci5wYXJzZUludCh0ZW1wby52YWx1ZSk7XG4gICAgfSk7XG59XG5jb25zdCB0aW1pbmcgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdzZWxlY3RbZGF0YS10aW1pbmddJyk7XG50aW1pbmcudmFsdWUgPSBzb25nLnRpbWluZztcbmlmICh0aW1pbmcpIHtcbiAgICB0aW1pbmcuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgICAgICBzb25nLnRpbWluZyA9IHRpbWluZy52YWx1ZTtcbiAgICB9KTtcbn1cbiIsImNvbnN0IGJhc3MgPSB7XG4gICAgd2F2ZTogJ3RyaWFuZ2xlJyxcbiAgICBub3RlczogW1xuICAgICAgICBbJ3InLCAxIC8gMl0sXG4gICAgICAgIFsncicsIDEgLyAyXSxcbiAgICAgICAgW1snYycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snYycsIDJdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydjJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydjJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snYScsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snYScsIDJdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydhJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydhJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snYicsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2InLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2MnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2MnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snYycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snYycsIDJdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydjJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydjJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snYScsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snYScsIDJdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydhJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydhJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snYicsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2InLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2MnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2MnLCAyXSwgMSAvIDhdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snZCcsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snZCcsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydhJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snYScsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snZCcsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2cnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snZCcsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydnJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2QnLCAyXSwgMSAvIDRdLFxuICAgICAgICBbWydkJywgMl0sIDEgLyA0XSxcbiAgICAgICAgW1snYycsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snZCcsIDJdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDJdLCAxIC8gNF0sXG4gICAgXVxufTtcbmV4cG9ydCBkZWZhdWx0IGJhc3M7XG4iLCJjb25zdCBsZWFkID0ge1xuICAgIHdhdmU6ICdzcXVhcmUnLFxuICAgIG5vdGVzOiBbXG4gICAgICAgIFsncicsIDEgLyAyXSxcbiAgICAgICAgWydyJywgMSAvIDRdLFxuICAgICAgICBbWydnJywgM10sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDNdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydnJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZicsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2UnLCA0XSwgMSAvIDRdLFxuICAgICAgICBbWydkJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snYycsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2cnLCAzXSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA4XSxcbiAgICAgICAgW1snYycsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2MnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgNF0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydkJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZCcsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2MnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydiJywgM10sIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDNdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydhJywgM10sIDEgLyA4XSxcbiAgICAgICAgW1snYScsIDNdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2EnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydhJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2YnLCA0XSwgMSAvIDRdLFxuICAgICAgICBbWydlJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZCcsIDRdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyAyXSxcbiAgICAgICAgW1snYicsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2EnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydnJywgNF0sIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2YnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydlJywgNF0sIDEgLyA0XSxcbiAgICAgICAgW1snZScsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydjJywgNF0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbJ3InLCAxIC8gMl0sXG4gICAgICAgIFtbJ2cnLCAzXSwgMSAvIDhdLFxuICAgICAgICBbWydnJywgM10sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydmJywgNF0sIDEgLyA0XSxcbiAgICAgICAgW1snZScsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2QnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydjJywgNF0sIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDNdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydjJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snYycsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCA0XSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgNF0sIDEgLyA0XSxcbiAgICAgICAgW1snYycsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2InLCAzXSwgMSAvIDRdLFxuICAgICAgICBbWydnJywgM10sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2EnLCAzXSwgMSAvIDhdLFxuICAgICAgICBbWydhJywgM10sIDEgLyA4XSxcbiAgICAgICAgW1snYScsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2EnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydnJywgNF0sIDEgLyA0XSxcbiAgICAgICAgW1snZicsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2UnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgNF0sIDEgLyA0XSxcbiAgICAgICAgWydyJywgMSAvIDJdLFxuICAgICAgICBbWydiJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snYScsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCA0XSwgMSAvIDRdLFxuICAgICAgICBbWydnJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZicsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2UnLCA0XSwgMSAvIDRdLFxuICAgICAgICBbWydlJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZCcsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2MnLCA0XSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFsncicsIDEgLyAyXSxcbiAgICAgICAgW1snYicsIDNdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2MnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZScsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCA0XSwgMSAvIDRdLFxuICAgICAgICBbWydiJywgM10sIDEgLyA0XSxcbiAgICAgICAgW1snZycsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCA0XSwgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gMl0sXG4gICAgICAgIFtbJ2InLCAzXSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snYycsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2MnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydiJywgM10sIDEgLyA4XSxcbiAgICAgICAgW1snYScsIDNdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2EnLCAzXSwgMSAvIDhdLFxuICAgICAgICBbWydjJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snYicsIDNdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyAyXSxcbiAgICAgICAgWydyJywgMSAvIDhdLFxuICAgICAgICBbWydiJywgM10sIDEgLyA4XSxcbiAgICAgICAgW1snYycsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2QnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydlJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZCcsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2InLCAzXSwgMSAvIDRdLFxuICAgICAgICBbWydnJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZCcsIDRdLCAxIC8gNF0sXG4gICAgICAgIFsncicsIDEgLyAyXSxcbiAgICAgICAgWydyJywgMSAvIDRdLFxuICAgICAgICBbJ3InLCAxIC8gOF0sXG4gICAgICAgIFtbJ2cnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydmcycsIDRdLCAxIC8gOF0sXG4gICAgICAgIFtbJ2UnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydmcycsIDRdLCAxIC8gNF0sXG4gICAgICAgIFtbJ2UnLCA0XSwgMSAvIDhdLFxuICAgICAgICBbWydkJywgNF0sIDEgLyA4XSxcbiAgICAgICAgW1snZycsIDRdLCAxIC8gNF0sXG4gICAgXSxcbn07XG5leHBvcnQgZGVmYXVsdCBsZWFkO1xuIiwiaW1wb3J0IGxlYWQgZnJvbSAnLi9sZWFkJztcbmltcG9ydCBiYXNzIGZyb20gJy4vYmFzcyc7XG5jb25zdCBzb25nID0ge1xuICAgIHRlbXBvOiAxNDAsXG4gICAgcmVzb2x1dGlvbjogMSAvIDgsXG4gICAgdGltaW5nOiAnc3dpbmcnLFxuICAgIHRyYWNrczogW1xuICAgICAgICBsZWFkLFxuICAgICAgICBiYXNzLFxuICAgIF0sXG4gICAgZHJ1bXM6IFsnaCcsICdoJywgJ3MnLCAnaCcsICdoJywgJ2gnLCAncycsICdoJ10sXG59O1xuZXhwb3J0IGRlZmF1bHQgc29uZztcbiJdLCJzb3VyY2VSb290IjoiIn0=