import lead from './lead';
import bass from './bass';

const song: SongDefinition = {
  tempo: 140,
  resolution: 1 / 8,
  timing: 'swing',
  tracks: [
    lead,
    bass,
  ],
  drums: ['h', 'h', 's', 'h', 'h', 'h', 's', 'h'],
};

export default song;
