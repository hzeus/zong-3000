# ZONG 3000 – Examples

## Instructions

Install necessary npm packages:

```
npm install
```

Start the Webpack dev server to serve the examples page:

```
npm run start
```
