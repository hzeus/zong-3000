# ZONG 3000 – 8bit music for the cyber future

## Examples

See [https://pmk1c.gitlab.io/zong-3000/](https://pmk1c.gitlab.io/zong-3000/) for examples.

## Installation

```
npm install zong-3000
```

## Usage

Create a song by importing the `Song` class and connecting it to an `AudioNode`.

```javascript
import { Song } from 'zong-3000';

const AudioContext = window.AudioContext || window.webkitAudioContext;
const context = new AudioContext();
const song = Song.new({
  // tempo of the song
  tempo: 140,

  // resolution of the drum pattern, and the smallest note value you can have in your tracks
  resolution: 1 / 8,

  // 'straight' or 'swing'
  timing: 'straight',

  // melodies played simulatnously
  tracks: [
    // notes with key and value
    // 'r': rest
    // 's': sharp
    [['c', 3], 1 / 4],
    [['cs', 3], 1 / 4],
    [['d', 3], 1 / 4],
    [['ds', 3], 1 / 4],
    [['e', 3], 1 / 4],
    [['f', 3], 1 / 4],
    [['fs', 3], 1 / 4],
    [['g', 3], 1 / 4],
    [['gs', 3], 1 / 4],
    [['a', 3], 1 / 4],
    [['as', 3], 1 / 4],
    [['b', 3], 1 / 4],
  ],

  // drum pattern
  // 's' = snare
  // 'h' = hi-hat
  // 0 = rest
  drums: ['s', 0, 'h', 0, 'h', 0, 'h', 0],
}, context);

// connect song
song.connect(context.destination);

// play song
song.play();

// pause song
song.pause();

// stop song
song.stop();

// change tempo (while playing)
song.tempo = 120;

// change timing (while playing)
song.timing = 'swing';
```

## Contribution

Feel free to create an issue or merge request.
I'd like to see some more examples, so if you have a song you want to show the world, just submit a merge request and add the tag `examples`.
